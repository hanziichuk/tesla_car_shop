package com.tesla.shop.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Wheels
{
	@Column(name = "wheels_name")
	private String code;
	@Column(name = "wheels_price")
	private BigDecimal price;

	public Wheels()
	{
	}

	public Wheels(String code, BigDecimal price)
	{
		this.code = code;
		this.price = price;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public BigDecimal getPrice()
	{
		return price;
	}

	public void setPrice(BigDecimal price)
	{
		this.price = price;
	}
}
