package com.tesla.shop.model;

import javax.persistence.*;


@Entity
@Table(name = "cars_configuration")
public class CarConfiguration
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "car_configuration_id")
	private int id;
	@Column(name = "car_configuration")
	private String configuration;
	@Column(name = "car_range")
	private Integer range;
	@Column(name = "car_top_speed")
	private Integer topSpeed;
	@Column(name = "car_acceleration")
	private Double acceleration;

	@ManyToOne
	@JoinColumn(name = "car_id", nullable = false)
	private Car car;

	public CarConfiguration()
	{
	}

	public CarConfiguration(String configuration, Integer range, Integer topSpeed, Double acceleration, Car car)
	{
		this.configuration = configuration;
		this.range = range;
		this.topSpeed = topSpeed;
		this.acceleration = acceleration;
		this.car = car;
	}

	public String getConfiguration()
	{
		return configuration;
	}

	public void setConfiguration(String configuration)
	{
		this.configuration = configuration;
	}

	public Integer getRange()
	{
		return range;
	}

	public void setRange(Integer range)
	{
		this.range = range;
	}

	public Integer getTopSpeed()
	{
		return topSpeed;
	}

	public void setTopSpeed(Integer topSpeed)
	{
		this.topSpeed = topSpeed;
	}

	public Double getAcceleration()
	{
		return acceleration;
	}

	public void setAcceleration(Double acceleration)
	{
		this.acceleration = acceleration;
	}

	public Car getCar()
	{
		return car;
	}

	public void setCar(Car car)
	{
		this.car = car;
	}
}
