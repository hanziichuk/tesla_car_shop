package com.tesla.shop.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "cars")
public class Car
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "car_id")
	private int id;
	@Column(name = "car_model")
	private String model;
	@Column(name = "car_price")
	private BigDecimal price;
	@Embedded
	private ColorCar carColor;
	@Embedded
	private Wheels wheels;
	@Embedded
	private CarRoof carRoof;
	@Column(name = "car_autopilot")
	private boolean autopilot;

	@OneToMany(mappedBy = "car", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<CarConfiguration> carsConfig = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Car()
	{
	}

	public Car(String model, BigDecimal price, ColorCar carColor, Wheels wheels, CarRoof carRoof, boolean autopilot,
               List<CarConfiguration> carsConfig, User user)
	{
		this.model = model;
		this.price = price;
		this.carColor = carColor;
		this.wheels = wheels;
		this.carRoof = carRoof;
		this.autopilot = autopilot;
		this.carsConfig = carsConfig;
		this.user = user;
	}

	public List<CarConfiguration> getCarsConfig()
	{
		return carsConfig;
	}

	public void setCarsConfig(List<CarConfiguration> carsConfig)
	{
		this.carsConfig = carsConfig;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getModel()
	{
		return model;
	}

	public void setModel(String model)
	{
		this.model = model;
	}

	public BigDecimal getPrice()
	{
		return price;
	}

	public void setPrice(BigDecimal price)
	{
		this.price = price;
	}

	public ColorCar getCarColor()
	{
		return carColor;
	}

	public void setCarColor(ColorCar carColor)
	{
		this.carColor = carColor;
	}

	public Wheels getWheels()
	{
		return wheels;
	}

	public void setWheels(Wheels wheels)
	{
		this.wheels = wheels;
	}

	public CarRoof getCarRoof()
	{
		return carRoof;
	}

	public void setCarRoof(CarRoof carRoof)
	{
		this.carRoof = carRoof;
	}

	public boolean isAutopilot()
	{
		return autopilot;
	}

	public void setAutopilot(boolean autopilot)
	{
		this.autopilot = autopilot;
	}
}
