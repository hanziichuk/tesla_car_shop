package com.tesla.shop.model.enums;

import javax.persistence.Embeddable;

@Embeddable
public enum Roof {
    GLASS_ROOF,SUN_ROOF
}
