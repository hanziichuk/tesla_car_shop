package com.tesla.shop.model.enums;

import javax.persistence.Embeddable;

@Embeddable
public enum Color {
    SOLID_BLACK,SILVER_METALLIC,OBSIDIAN_BLACK,DEEP_BLUE,PEARL_WHITE,RED
}
