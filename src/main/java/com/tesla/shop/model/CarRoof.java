package com.tesla.shop.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import com.tesla.shop.model.enums.Roof;


@Embeddable
public class CarRoof
{
	@Embedded
	@Column(name = "car_roof")
	private Roof roof;

	@Column(name = "roof_price")
	private BigDecimal roofPrice;

	public CarRoof(Roof roof, BigDecimal roofPrice)
	{
		this.roof = roof;
		this.roofPrice = roofPrice;
	}

	public CarRoof()
	{
	}

	public Roof getRoof()
	{
		return roof;
	}

	public void setRoof(Roof roof)
	{
		this.roof = roof;
	}

	public BigDecimal getRoofPrice()
	{
		return roofPrice;
	}

	public void setRoofPrice(BigDecimal roofPrice)
	{
		this.roofPrice = roofPrice;
	}
}
