package com.tesla.shop.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import com.tesla.shop.model.enums.Color;


@Embeddable
public class ColorCar
{
	@Embedded
	@Column(name = "car_color")
	private Color color;
	@Column(name = "color_price")
	private BigDecimal colorPrice;

	public ColorCar()
	{
	}

	public ColorCar(Color color, BigDecimal colorPrice)
	{
		this.color = color;
		this.colorPrice = colorPrice;
	}

	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public BigDecimal getColorPrice()
	{
		return colorPrice;
	}

	public void setColorPrice(BigDecimal colorPrice)
	{
		this.colorPrice = colorPrice;
	}
}
