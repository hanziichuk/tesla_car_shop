package com.tesla.shop.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "users")
public class User
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id",nullable = false)
	private int id;
	@Column(name = "user_name")
	private String name;
	@Column(name = "user_phone")
	private String phone;
	@Column(name = "user_email")
	private String email;
	@Column(name = "user_password")
	private String password;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Car> cars = new ArrayList<>();

	public User()
	{
	}

	public User(String name)
	{
		this.name = name;
	}

	public User(String name, String phone, String email, String password, List<Car> cars)
	{
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.password = password;
		this.cars = cars;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public List<Car> getCars()
	{
		return cars;
	}

	public void setCars(List<Car> cars)
	{
		this.cars = cars;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
